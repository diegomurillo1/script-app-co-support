
# Importaciones para variables de entorno
import os
from dotenv import dotenv_values

# Importación de Slack y el Excel
from slack_sdk import WebClient
import openpyxl

#Importación de fecha
from datetime import datetime
# Token de OAuth de Slack

env_vars = dotenv_values(".env")
slack_token = env_vars.get("SLACK_TOKEN")
slack_channel = env_vars.get("SLACK_CHANNEL")
# Crea una instancia del cliente de la API de Slack
client = WebClient(token=slack_token)

def get_message_date(timestamp):
    # Convertir el timestamp a una fecha legible
    timestamp = float(timestamp)  # Convertir a número flotante
    date = datetime.utcfromtimestamp(timestamp).strftime("%Y-%m-%d %H:%M:%S")
    return date

def get_channel_messages(channel_id):
    # Parámetros de paginación para obtener todos los mensajes del canal
    limit = 100
    cursor = None
    messages = []
    while True:
        # Realiza la llamada a la API para obtener mensajes del canal
        response = client.conversations_history(
            channel=channel_id,
            limit=limit,
            cursor=cursor
        )
        if response["ok"]:
            messages.extend(response["messages"])
            aux = response["messages"]
            if response["has_more"]:
                cursor = response["response_metadata"]["next_cursor"]
            else:
                break
        else:
            print(f"Error: {response['error']}")
    return messages

def get_username(user_id):
    response = client.users_info(user=user_id)
    if response["ok"]:
        user_info = response["user"]
        username = user_info["real_name"]
        return username
    else:
        print(f"Error al obtener la información del usuario: {response['error']}")
        return None

def see_messages():
    messages = get_channel_messages(slack_channel)
    workbook = openpyxl.Workbook()
    sheet = workbook.active
    row = 2
    # Agregar encabezados de columnas
    sheet["A1"] = "Usuario"
    sheet["B1"] = "Mensaje"
    sheet["C1"] = "Fecha"
    for message in messages:
        text_message = message['text']
        user_message = message['user']
        timestamp = message['ts']
        
        
        # Obtener el nombre del usuario
        username_message = get_username(user_message)
        
        # Obtener la fecha del mensaje
        message_date = get_message_date(timestamp)
        
        # Agregar los datos a las columnas correspondientes
        sheet.cell(row=row, column=1).value = username_message
        sheet.cell(row=row, column=2).value = text_message
        sheet.cell(row=row, column=3).value = message_date
        
        row += 1
        
    workbook.save("support_messages.xlsx")
if __name__ == '__main__':
    see_messages()